package sinemayk.pageobjects;

import sinemayk.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {
        public void fillInUserName(WebDriver driver, String s){
        WebUtil.clearAndSendKeys(driver, By.id("identifierId"),s);

}
        public PasswordPage userNameClick(WebDriver driver) {
        WebUtil.click(driver,By.xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/content"));

        return PageFactory.initElements(driver, PasswordPage.class);
    }

}
