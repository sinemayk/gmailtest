package sinemayk.pageobjects;

import sinemayk.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class EmailHomePage {
    public static SignInPage signOut(WebDriver driver) {
        WebUtil.click(driver,By.xpath("//a[@class='gb_x gb_Da gb_f']"));

        WebUtil.click(driver,By.linkText("Oturumu kapat"));

        return PageFactory.initElements(driver,SignInPage.class);
    }

    public static boolean isInboxExist(WebDriver driver) {
        return WebUtil.isElementExist(driver,By.partialLinkText("Gelen Kutusu"));

    }

    public static EmailViewPage clickNewEmail(WebDriver driver) {
        WebUtil.waitForElementVisible(driver,By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div[1]/div/div[6]/div/div[1]/div/table/tbody/tr[1]/td[6]/div/div/div/span/span"));
        WebUtil.click(driver,By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div[1]/div/div[6]/div/div[1]/div/table/tbody/tr[1]/td[6]/div/div/div/span/span"));
        WebUtil.waitForElementVisible(driver,By.xpath("/html[1]/body[1]/div[7]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tr[1]/td[1]/div[2]/div[1]/div[2]/div[1]/h2[1]"));
        return PageFactory.initElements(driver,EmailViewPage.class);
    }

    public void clickComposeButton(WebDriver driver) {
        WebUtil.click(driver,By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']") );

    }
    public void fillInRecipient(WebDriver driver, String s){
        WebUtil.waitForElementVisible(driver,By.xpath("//textarea[@name='to']"));
        WebUtil.clearAndSendKeys(driver,By.xpath("//textarea[@name='to']"),s);

    }
    public void fillInSubject(WebDriver driver,String s) {
        WebUtil.clearAndSendKeys(driver,By.cssSelector("input[name='subjectbox'"),s);
    }


    public void fillInBody(WebDriver driver,String s) {
        WebUtil.clearAndSendKeys(driver,By.cssSelector("div[class='Am Al editable LW-avf']"),s);
    }

    public void clickGonder(WebDriver driver) {
        WebUtil.click(driver,By.xpath("//div[contains(@class,'T-I J-J5-Ji aoO v7 T-I-atl L3')]"));//maven türkçeyi sevmiyor
    }

    public void clickGelenKutusu(WebDriver driver) {
        WebUtil.click(driver,By.partialLinkText("Gelen Kutusu"));
    }

    public void deleteNewMail(WebDriver driver) throws Exception{

        WebUtil.click(driver,By.xpath("/html[1]/body[1]/div[7]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]"));
        Thread.sleep(2000);
    }
}
