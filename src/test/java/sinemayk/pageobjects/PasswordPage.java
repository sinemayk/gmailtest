package sinemayk.pageobjects;

import sinemayk.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PasswordPage {
    public void fillInPassword(WebDriver driver, String s){
        WebUtil.clearAndSendKeys(driver,By.xpath("//input[contains(@type,'password')]"),s);

    }

    public EmailHomePage passwordClick(WebDriver driver) {
        WebUtil.click(driver,By.xpath("//span[contains(@class,'RveJvd snByac')]"));
        WebUtil.waitForElementVisible(driver,By.partialLinkText("Gelen Kutusu"));
        return PageFactory.initElements(driver, EmailHomePage.class);
    }
}

