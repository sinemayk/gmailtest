package sinemayk.pageobjects;

import sinemayk.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmailViewPage {
    public String getEmailSubject(WebDriver driver) {
       return WebUtil.getElementText(driver, By.xpath("/html[1]/body[1]/div[7]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tr[1]/td[1]/div[2]/div[1]/div[2]/div[1]/h2[1]"));

    }

    public String getBodyText(WebDriver driver) {
        return WebUtil.getElementText(driver, By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[1]"));

    }
}
