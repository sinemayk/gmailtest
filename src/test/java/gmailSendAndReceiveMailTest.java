import org.junit.experimental.categories.Category;
import sinemayk.categories.Critical;
import sinemayk.categories.Major;
import sinemayk.pageobjects.EmailHomePage;
import sinemayk.pageobjects.EmailViewPage;
import sinemayk.pageobjects.PasswordPage;
import sinemayk.pageobjects.SignInPage;
import sinemayk.util.WebUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class gmailSendAndReceiveMailTest {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
    }
    @Category({Critical.class})
    @Test
    public void gmailSendAndReceiveEmailShouldBeSuccessful() throws Exception{

        SignInPage signInPage= WebUtil.goToSignInPage(driver);
        signInPage.fillInUserName(driver, "sinemayk@gmail.com");
        PasswordPage passwordPage = signInPage.userNameClick(driver);//burada passwordpage e geçiyor
        WebUtil.waitForElementVisible(driver, By.xpath("//input[contains(@type,'password')]"));
        passwordPage.fillInPassword(driver, "19091207ks");
        EmailHomePage emailHomePage = passwordPage.passwordClick(driver);//burada emailhome a geçiyor


//Aşağıda findElements kullanılmasının sebebi gelen kutusunun bir tabloya bağlı olması, bu tabloda bulunan en az bir element olmalı o da gelenkutusu, div/span/a
//a olarak belirtildiğinde link oluyor(linkage), bunda partialLinkText kullanılabilir

        Assert.assertTrue("Inbox should exist", EmailHomePage.isInboxExist(driver));

        emailHomePage.clickComposeButton (driver);
        emailHomePage.fillInRecipient(driver,"sinemayk@gmail.com");

        final String subject = "Gmail Send Email Test";
        emailHomePage.fillInSubject(driver,subject);


        final String body = "Hello Testers Good Morning";
        emailHomePage.fillInBody(driver,body);

        emailHomePage.clickGonder(driver);

        Thread.sleep(10000);
        //WebUtil.waitForElementVisible(driver,(By.cssSelector("a[aria-label='Gelen Kutusu 1 okunmamış']")));

        emailHomePage.clickGelenKutusu(driver);


        EmailViewPage emailViewPage =EmailHomePage.clickNewEmail(driver);//emailview e geçiyor

        String actualSubject = emailViewPage.getEmailSubject(driver);
        Assert.assertEquals("Email subject should be the same", subject,actualSubject);

        String actualBodyText = emailViewPage.getBodyText(driver);
        Assert.assertEquals("Body text should be the same", body,actualBodyText );

        emailHomePage.deleteNewMail(driver);//ekranda açık olan yeni atılmış maili siliyor

        WebUtil.waitForElementVisible(driver, By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div"));

        signInPage = EmailHomePage.signOut(driver);


    }

    @After
    public void tearDown() {
        driver.quit();
    }
}